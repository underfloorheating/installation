<?php
/**
 * @version     $Id$
 * @package     Ufhs_Installation
 * @author      Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */

$installer = $this;

$installer->startSetup();

$installer->run("
	DROP TABLE IF EXISTS {$installer->getTable('installation/customer')};

	CREATE TABLE {$installer->getTable('installation/customer')} (
	`id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`status_id` int(10) UNSIGNED NOT NULL,
	`first_name` VARCHAR(255) NOT NULL,
	`last_name` VARCHAR(255) NOT NULL,
	`email` VARCHAR(255) NOT NULL,
	`telephone` VARCHAR(20) NOT NULL,
	`address1` VARCHAR(255) NOT NULL,
	`address2` VARCHAR(255) NOT NULL DEFAULT '',
	`town` VARCHAR(255) NOT NULL,
	`postcode` VARCHAR(20) NOT NULL,
	`property` VARCHAR(255) NOT NULL,
	`project_type` VARCHAR(255) NOT NULL,
	`additional_info` VARCHAR(4000) NOT NULL DEFAULT '',
	`total_cost` FLOAT(10) UNSIGNED NOT NULL DEFAULT 0.00,
	`total_quoted` FLOAT(10) UNSIGNED NOT NULL DEFAULT 0.00,
	`last_modified` timestamp NOT NULL,
	`state_change` datetime NOT NULL,
	`created_date` datetime NOT NULL,
	PRIMARY KEY (`id`)
	)
	COLLATE='latin1_swedish_ci'
	ENGINE=InnoDB
	;
	");

$installer->run("
	DROP TABLE IF EXISTS {$installer->getTable('installation/room')};

	CREATE TABLE {$installer->getTable('installation/room')} (
	`id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`quote_id` int(10) UNSIGNED NOT NULL,
	`room_name` VARCHAR(255) NOT NULL,
	`subfloor` VARCHAR(255) NOT NULL,
	`ceiling_height` VARCHAR(20) NOT NULL,
	`number_of_windows` int(10) UNSIGNED NOT NULL,
	`floor_area` VARCHAR(20) NOT NULL,
	`floor_finish` VARCHAR(50) NOT NULL,
	`wall_type` VARCHAR(20) NOT NULL,
	PRIMARY KEY (`id`)
	)
	COLLATE='latin1_swedish_ci'
	ENGINE=InnoDB
	;
	");

$installer->run("
	DROP TABLE IF EXISTS {$installer->getTable('installation/additions')};

	CREATE TABLE {$installer->getTable('installation/additions')} (
	`id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`fkid` int(10) UNSIGNED NOT NULL,
	`type` VARCHAR(255) NOT NULL,
	`content` VARCHAR(4000) NOT NULL,
	`timestamp` timestamp NOT NULL,
	`userstamp` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`id`)
	)
	COLLATE='latin1_swedish_ci'
	ENGINE=InnoDB
	;
	");

$installer->run("
	DROP TABLE IF EXISTS {$installer->getTable('installation/status')};

	CREATE TABLE {$installer->getTable('installation/status')} (
	`id` int(10) UNSIGNED NOT NULL,
	`status_text` VARCHAR(100) NOT NULL,
	`status_code` VARCHAR(100) NOT NULL,
	`active` tinyint(1) NOT NULL DEFAULT '1',
	PRIMARY KEY (`id`)
	)
	COLLATE='latin1_swedish_ci'
	ENGINE=InnoDB
	;
	INSERT INTO {$installer->getTable('installation/status')} (`id`, `status_text`, `status_code`, `active`) VALUES (0, 'Awaiting Confirmation', 'awaiting_confirmation', 1), (1, 'With Surveyor', 'with_surveyor', 1), (2, 'Quote Required', 'quote_required', 1), (3, 'Quote With Customer', 'quote_with_customer', 1), (4, 'Awaiting Deposit', 'awaiting_deposit', 0), (5, 'Ready To Install', 'ready_to_install', 0), (6, 'Installation In Progress', 'installation_in_progress', 1), (7, 'Ready For Billing', 'ready_for_billing', 1), (8, 'Complete', 'complete', 1), (9, 'Cancelled', 'cancelled', 1), (10, 'Deleted', 'deleted', 1), (11, 'With Accounts', 'with_accounts', 1);
	");

$installer->run("
	DROP TABLE IF EXISTS {$installer->getTable('installation/advisors')};

	CREATE TABLE {$installer->getTable('installation/advisors')} (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`name` varchar(400) DEFAULT NULL,
	`telephone` varchar(20) DEFAULT NULL,
	`email` varchar(200) DEFAULT NULL,
	PRIMARY KEY (`id`)
	) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

	INSERT INTO {$installer->getTable('installation/advisors')} (`id`, `name`, `telephone`, `email`) VALUES (1, 'Default Salesman', '01268567016', 'sales@theunderfloorheatingstore.com');
	");

$installer->run("
	DROP TABLE IF EXISTS {$installer->getTable('installation/installers')};

	CREATE TABLE {$installer->getTable('installation/installers')} (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`name` varchar(400) DEFAULT NULL,
	`email` varchar(200) DEFAULT NULL,
	`telephone` varchar(20) NOT NULL DEFAULT '',
	`user_id` int(11) DEFAULT NULL,
	`company` varchar(400) DEFAULT NULL,
	`address1` varchar(255) NOT NULL DEFAULT '',
	`address2` varchar(255) NOT NULL DEFAULT '',
	`town` varchar(255) NOT NULL DEFAULT '',
	`postcode` varchar(20) NOT NULL DEFAULT '',
	`county` varchar(255) NOT NULL DEFAULT '',
	`mobile` varchar(20) NOT NULL DEFAULT '',
	PRIMARY KEY (`id`)
	) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

	INSERT INTO {$installer->getTable('installation/installers')} (`id`, `name`, `email`, `telephone`, `user_id`, `company`, `address1`, `address2`, `town`, `postcode`, `county`, `mobile`) VALUES (1, 'Bill Shelton', 'bill.shelton07@btinternet.com', '01375 840477', 58, 'Pureheat ltd', '44 London Road', '', 'Tilbury', 'RM18 8DU', 'Essex', '07983431859');
	");

$installer->endSetup();