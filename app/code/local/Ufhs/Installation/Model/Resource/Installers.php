<?php

/**
 * @version     $Id$
 * @package     Ufhs_Installation
 * @author      Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */
class Ufhs_Installation_Model_Resource_Installers extends Mage_Core_Model_Resource_Db_Abstract
{

    public function _construct()
    {
        $this->_init('installation/installers', 'id');
    }

}