<?php

/**
 * @version $Id$
 * @package Ufhs_Installation
 * @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */

class Ufhs_Installation_Model_Post extends Mage_Core_Model_Abstract
{
	public $errors = [];
	private $regexRules = [
	'first_name' => '/^[A-Za-z\.\- ,]*$/',
	'last_name' => '/^[A-Za-z\.\- ]*$/',
	'email' => '/^[^@]+@[^@]+\.[a-zA-Z]{2,6}$/',
	'telephone' => '/^[0-9+() ]{11,16}$/',
	'address1' => '/^[0-9A-Za-z\.\- ,]*$/',
	'address2' => '/^[0-9A-Za-z\.\- ,]*$/',
	'town' => '/^[0-9A-Za-z\.\- ,]*$/',
	'postcode' => '/^[A-Z]{1,2}[0-9][0-9A-Z]?\s?[0-9][A-Z]{2}$/i',
	'property' => '/^[A-Za-z]*$/',
	'project_type' => '/^[A-Za-z]*$/',
	'optional_fields' => ['address2','additional_info']
	];
	/**
	 * Validate Post
	 * -------------
	 * Take the post, supplied by an array, and run the regex rules agaisnt each
	 * field, adding entries to the error array is applicable.
	 *
	 * @param	$post	Array	The post data.
	 */
	public function validatePost($post)
	{
		$this->errors = [];
		foreach($post as $key => $value)
		{
			if(strlen($value) == 0 && !in_array($key, $this->regexRules['optional_fields']) && array_key_exists($key, $this->regexRules)) {
				$this->errors[$key] = 'A value is required';
			} else if(!isset($this->regexRules[$key])) {
				continue;
			} else if(!preg_match($this->regexRules[$key], $value)) {
				$this->errors[$key] = "Invalid value";
			}
		}
	}
	/**
	 * Has Error
	 * ---------
	 * Check the error array to see if we have any errors.
	 *
	 * @return	Boolean	Do we have errors.
	 */
	public function hasErrors()
	{
		return !empty($this->errors);
	}
	/**
	 * Generate Error JSON
	 * -------------------
	 * Take the errors array and generate an error json to be outtputed on the frontend.
	 *
	 * @return	String	The JSON encoded errors.
	 */
	public function generateErrorJson()
	{
		$return['status'] = 'failed';
		$return['type'] = 'validation';
		foreach($this->errors as $key => $value)
		{
			$return['errors'][] = [
			'fieldName' => $key,
			'error' => $value
			];
		}
		return json_encode($return);
	}
	/**
	 * Save Rooms
	 * ----------
	 * Take the post data and decode and build rooms based on the rooms index.
	 *
	 * @param	$post	Array	The post data.
	 * @param	$customerId	String	The related customer ID.
	 * @return	Boolean	Has the rooms been saved successfully.
	 */
	public function saveRooms($post,$customerId)
	{
		$rooms = json_decode($post['rooms'],true);
		if(!$rooms)
		{
			return false;
		}
		foreach($rooms as $room)
		{
			$room['quote_id'] = $customerId;
			$roomDB = Mage::getModel('installation/room')->setData($room)->save();
		}
		return true;
	}
	/**
	 * Save Files
	 * ----------
	 * For each of the $files ($_FILES) posted files, isntantiate a varien_file_uploader
	 * object and attempt to save the files to drawing directory for that particular
	 * customer.
	 *
	 * @param	$files	Array	An array conaining the files. ($_FILES)
	 * @param	$id	Int	The customer ID.
	 * @return	Boolean	Have we had any problem saving the files.
	 */
	public function saveFiles($files,$id)
	{
		$mediaPath = Mage::getBaseDir('media') . '/ufhs/installation/drawing/' . $id;
		foreach($files as $key => $value)
		{
			if($value['error'] != 0)
			{
				continue;
			}
			$files[$key]['name'] = strtolower(str_replace([" ","&","?"],"-",$_FILES[$key]['name']));
			$uploader = Mage::helper('installation')->upload_handle($key);
			if(!$uploader->save($mediaPath, $files[$key]['name']))
			{
				Mage::getSingleton('core/session')->addError('Failed to upload ' . $_FILES[$key]['name'] . ', please try again.');
				return false;
			}
			else
			{
				Mage::getModel('installation/drawing')->setFK($id)
				->setContent($files[$key]['name'])
				->save();
			}
		}
		return true;
	}
}