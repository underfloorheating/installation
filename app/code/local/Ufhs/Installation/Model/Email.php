<?php

/**
 * @version $Id$
 * @package Ufhs_Installation
 * @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */

class Ufhs_Installation_Model_Email extends Mage_Core_Model_Abstract
{
    public function getAddr($to, $survey = FALSE)
    {
        // $to either install_manager, dev, or id if $survey == TRUE
        if ($survey) {
            return $this->getInstallerEmail($to);
        }
        if ($to == 'install_manager') {
            return $this->getInstallManagerEmail();
        }
        return Mage::getStoreConfig('installation_emails/emails/' . $to);
    }

    /**
     * Email
     * -----
     *
     * @param   $addr   String  The address we want to email.
     * @param   $message    String  The message we want to send.
     */
    public function send($addr,$message, $subject = 'Installation Service')
    {
        $mail = Mage::getModel('core/email')
        ->setToName('The Underfloor Heating Store')
        ->setToEmail($addr)
        ->setBody($message)
        ->setSubject($subject)
        ->setFromEmail('no-reply@theunderfloorheatingstore.com')
        ->setFromName('UFHS installation service')
        ->setType('html');

        try {
            if(!$mail->send()) {
                throw new Exception('Unable to send.');
            }
        }
        catch (Exception $e) {
            throw new Exception($e->getMessages());
        }
    }

    public function getEmailBody($id, $status, $name, $message)
    {
        $data = [
        'id'=> $id,
        'status' => $status,
        'customer' => $name,
        'message' => $message
        ];
        return Mage::app()
        ->getLayout()
        ->createBlock('core/template')
        ->setTemplate('installation/email-template.phtml')
        ->setContent($data)
        ->toHtml();
    }

    public function getInstallerEmail($quoteId)
    {
        $installer = Mage::getModel('installation/customer')->load($quoteId)->getInstallerId();
        return Mage::getModel('installation/installers')->load($installer)->getEmail();
    }

    public function getInstallManagerEmail()
    {
        $installMgrId = Mage::getModel('admin/roles')->getCollection()->addFieldToFilter('role_name','Install Manager')->getAllIds()[0];
        $managers = Mage::getModel('admin/roles')->load($installMgrId)->getRoleUsers();
        $emails = [];
        foreach ($managers as $id) {
            $emails[] = Mage::getModel('admin/user')->load($id)->getEmail();
        }
        return empty($emails) ? Mage::getStoreConfig('installation_emails/emails/install_manager') : $emails;
    }
}