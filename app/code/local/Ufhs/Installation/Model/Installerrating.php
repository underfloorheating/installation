<?php

/**
 *
 * @version     $Id$
 * @package     Ufhs_Installation
 * @author      Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 *
 */
class Ufhs_Installation_Model_Installerrating extends Mage_Core_Model_Abstract
{
    private function _getJobCollection($id)
    {
        return Mage::getModel('installation/customer')->getCollection()->addFieldToFilter('installer_id', $id);
    }

    public function getReliabilityMatrix($id)
    {
        $outstanding = count($this->_getJobCollection($id)->addFieldToFilter('status_id', [1,6])->getData());
        $completed = count($this->_getJobCollection($id)->addFieldToFilter('status_id', 8)->getData());

        $total = $outstanding + $completed;
        $return['total_jobs'] = $total;
        $return['total_completed'] = $completed;
        $return['total_outstanding'] = $outstanding;
        $return['rating'] = ($completed === 0 || $total === 0) ? '0' : number_format(($completed / $total) * 100, 2);
        return $return;
    }

    public function getServiceMatrix($id)
    {
        $completed = count($this->_getJobCollection($id)->addFieldToFilter('status_id', 8)->getData());
        $cancelled = count($this->_getJobCollection($id)->addFieldToFilter('status_id', 9)->getData());
        $finished = $cancelled + $completed;

        $return['total_completed'] = $completed;
        $return['total_cancelled'] = $cancelled;
        $return['total_finished'] = $finished;
        $return['rating'] = ($completed === 0 || $finished === 0) ? '0' : number_format(($completed / $finished) * 100, 2);
        return $return;
    }

    public function getProfitMatrix($id)
    {
        $cost = array_reduce($this->_getJobCollection($id)->getData(), function($sum, $item){
            $sum += $item['total_cost'];
            return $sum;
        });
        $revenue = array_reduce($this->_getJobCollection($id)->getData(), function($sum, $item){
            $sum += $item['total_quoted'];
            return $sum;
        });
        $profit = $revenue - $cost;

        $return['total_installer_cost'] = $cost ?: '0';
        $return['total_job_revenue'] = $revenue ?: '0';
        $return['total_profit'] = $profit;
        $return['profit_rating'] = ($profit === 0 || $revenue === 0) ? '0' : number_format(($profit / $revenue) * 100, 2);
        return $return;
    }

    public function getOverallRating($id)
    {
        $reliability = $this->getReliabilityMatrix($id)['rating'];
        $service = $this->getServiceMatrix($id)['rating'];
        $profit = $this->getProfitMatrix($id)['profit_rating'];
        return ($profit + $reliability + $service === 0) ? 0 : number_format(($profit + $reliability + $service) / 3, 0);
    }

    public function getRanking($id)
    {
        $leaderboard = [];
        $collection = Mage::getModel('installation/installers')->getCollection()->addFieldToSelect('id')->getAllIds();
        foreach ($collection as $installer) {
            $leaderboard[$installer] = intval($this->getOverallRating($installer));
        }
        arsort($leaderboard);
        return (array_search($id, array_keys($leaderboard)) + 1) . ' / ' . count($leaderboard);
    }
}