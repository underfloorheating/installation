<?php

/**
 * Installation Post Controller
 *
 * @author  Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */
class Ufhs_Installation_PostController extends Mage_Core_Controller_Front_Action
{
	private $postModel;

	private function _getModel ()
	{
		if(!$this->postModel instanceof Ufhs_Installation_Model_Post)
		{
			$this->postModel = Mage::getModel('installation/post');
		}
		return $this->postModel;
	}

	public function indexAction()
	{
		$postModel = $this->_getModel();
		$postData = Mage::app()->getRequest()->getParams();
		$email = Mage::getModel('installation/email');
		// If we have post data
		if(!empty($postData)) {
			// Validate it and check for errors
			$postData = Mage::helper('installation')->url_decode_array($postData);
			$postModel->validatePost($postData);
			// If we don't have errors
			if(!$postModel->hasErrors()) {
				// Try to save the customer, rooms and files
				try {
					$customer = Mage::getModel('installation/customer');
					$postData['state_change'] = $postData['created_date'] = date('Y-m-d H:i:s');
					$customer->setData($postData);
					// If we start accepting room data again, this will ned re adding
					// if($customer->save() && $postModel->saveRooms($postData,$customer->getId()) && $postModel->saveFiles($_FILES,$customer->getId()))
					if($customer->save() && $postModel->saveFiles($_FILES, $customer->getId())) {
						$message = $email->getEmailBody(
							$customer->getId(),
							'Awaiting Confirmation',
							$customer->getFirstName() . ' ' . $customer->getLastName(),
							'A new installatioon request has come in.'
							);
						$toAddr = $email->getAddr('install_manager');
						$email->send($toAddr, $message, 'Installation Job Update - Awaiting Confirmation (#' . $customer->getId() . ')');
						echo json_encode(['status' => 'success']);
					} else {
						Mage::throwException("There was a problem saving an installation service record to the database.");
					}
				} catch (Exception $e) {
					// If an error is thrown, 99.99% of the time it's going to be a disallowed file type on the file, so return a json error with the error message
					$return['status'] = 'failed';
					$return['type'] = 'exception';
					$return['error'] = 'There was a problem and our team have been notified.  If you haven\'t heard back from us within 48 hours, give us a call on 01268 567016 to arrange your installation.';
					echo json_encode($return);

					$body = $e->getMessage() . PHP_EOL . PHP_EOL . $e->getTraceAsString();
					$id = $customer->getId() ?: 0;
					$message = $email->getEmailBody(
						$id,
						'An error has occured',
						$customer->getFirstName() . ' ' . $customer->getLastName(),
						$body
						);
					$toAddr = $email->getAddr('dev');
					$email->send($toAddr, $message, 'An error has occured with the Installation Service');
				}
			} else {
				// If we have errors, json encode them and return it
				echo $postModel->generateErrorJson();
			}
		} else {
			// If we don't have post data, return an error
			echo json_encode([
				"status" => "unauthorised",
				"errors" => "Where do you think you're going!"
				]
				);
		}

	}
}