<?php

/**
 * Installation Data Notes
 *
* @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */
class Ufhs_Installation_Block_Adminhtml_Installers_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $id = $this->getRequest()->getParam('id');
        $installer = Mage::getModel('installation/installers')->load($id);
        $form = new Varien_Data_Form(array(
            'id' => 'installeredit',
            'action' => $this->getUrl('*/*/installerupdate', ['id' => $id]),
            'method' => 'post',
            'enctype' => 'multipart/form-data'
            )
        );
        $form->setUseContainer(true);
        $this->setForm($form);

        $fieldset = $form->addFieldset('editcustomer_form', ['legend' => Mage::helper('installation')->__('Details')]);

        $fieldset->addField('name', 'text', [
            'label' => Mage::helper('installation')->__('Name'),
            'name' => 'name',
            'value' => $installer->getName() ?: '',
            'required' => true
            ]);
        $fieldset->addField('email', 'text', [
            'label' => Mage::helper('installation')->__('Email'),
            'name' => 'email',
            'value' => $installer->getEmail() ?: '',
            'required' => true
            ]);
        $fieldset->addField('telephone', 'text', [
            'label' => Mage::helper('installation')->__('Telephone'),
            'name' => 'telephone',
            'value' => $installer->getTelephone() ?: '',
            'required' => false
            ]);
        $fieldset->addField('mobile', 'text', [
            'label' => Mage::helper('installation')->__('Mobile'),
            'name' => 'mobile',
            'value' => $installer->getMobile() ?: '',
            'required' => false
            ]);
        $fieldset->addField('user_id', 'select', [
            'label' => Mage::helper('installation')->__('Magento User'),
            'name' => 'user_id',
            'value' => $installer->getUserId() ?: '',
            'values' => $this->_getUserIds(),
            'required' => true
            ]);
        $fieldset->addField('company', 'text', [
            'label' => Mage::helper('installation')->__('Company'),
            'name' => 'company',
            'value' => $installer->getCompany() ?: '',
            'required' => true
            ]);
        $fieldset->addField('address1', 'text', [
            'label' => Mage::helper('installation')->__('Address 1'),
            'name' => 'address1',
            'value' => $installer->getAddress1() ?: '',
            'required' => true
            ]);
        $fieldset->addField('address2', 'text', [
            'label' => Mage::helper('installation')->__('Address 2'),
            'name' => 'address2',
            'value' => $installer->getAddress2() ?: '',
            'required' => false
            ]);
        $fieldset->addField('town', 'text', [
            'label' => Mage::helper('installation')->__('Town'),
            'name' => 'town',
            'value' => $installer->getTown() ?: '',
            'required' => true
            ]);
        $fieldset->addField('postcode', 'text', [
            'label' => Mage::helper('installation')->__('Postcode'),
            'name' => 'postcode',
            'value' => $installer->getPostcode() ?: '',
            'required' => true
            ]);
        $fieldset->addField('county', 'text', [
            'label' => Mage::helper('installation')->__('County'),
            'name' => 'county',
            'value' => $installer->getCounty() ?: '',
            'required' => true
            ]);

        return parent::_prepareForm();
    }

    private function _getUserIds()
    {
        $collection = Mage::getModel('admin/user')->getCollection();
        $return = [];
        foreach ($collection as $user) {
            $role = Mage::getModel('admin/user')->load($user['user_id'])->getRole()->getData()['role_name'];
            if ($role == 'Installer') {
                $return[$user['user_id']] = $user['username'];
            }
        }
        return $return;
    }
}