<?php

/**
 * Installation Data Grid
 *
* @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */
class Ufhs_Installation_Block_Adminhtml_Installers_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function __construct()
    {
        parent::__construct();
        $id = $this->getRequest()->getParam('id');
        $this->_objectId = 'id';
        $this->_blockGroup = 'installation';
        $this->_controller = 'adminhtml_installers';
        $this->_mode = 'edit';

        $this->_removeButton('save');
        $this->_removeButton('delete');
        $this->_removeButton('back');
        $this->_removeButton('reset');

        $this->addButton('new_back', [
            'label' => 'Back',
            'onclick' => "setLocation('" . $this->getUrl('*/*/viewinstallers') . "')",
            'class' => 'back'
            ]);

        if ($this->getRequest()->getParam('id') > 0) {
            $buttonText = 'Update';
        } else {
            $buttonText = 'Save';
        }

        $this->addButton('new_save', [
            'label' => $buttonText,
            'onclick' => "document.getElementById('installeredit').submit()",
            'class' => 'add'
            ]);
    }

    public function getHeaderText()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            return Mage::helper('installation')->__('Edit Installer');
        } else {
            return Mage::helper('installation')->__('Add Installer');
        }

    }
}