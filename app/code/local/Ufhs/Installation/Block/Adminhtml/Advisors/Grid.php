<?php

/**
 * Installation Data Grid
 *
* @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */
class Ufhs_Installation_Block_Adminhtml_Advisors_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('advisorGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('installation/advisors')->getCollection();

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header' => Mage::helper('installation')->__('ID'),
            'align' => 'left',
            'width' => '10px',
            'index' => 'id'
            ));
        $this->addColumn('name', array(
            'header' => Mage::helper('installation')->__('Name'),
            'align' => 'left',
            'width' => '10px',
            'index' => 'name'
            ));
        $this->addColumn('email', array(
            'header' => Mage::helper('installation')->__('Email'),
            'align' => 'left',
            'width' => '10px',
            'index' => 'email'
            ));
        $this->addColumn('telephone', array(
            'header' => Mage::helper('installation')->__('Telephone'),
            'align' => 'left',
            'width' => '10px',
            'index' => 'telephone'
            ));

        $object = new Varien_Object(array('grid_block' => $this));
        Mage::dispatchEvent("installation_block_adminhtml_advisor_grid_preparecolumns", array("data" => $object));

        $this->addExportType('*/*/exportCsv', Mage::helper('installation')->__('CSV'));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/advisoredit', array('id' => $row->getData()['id']));
    }
}

