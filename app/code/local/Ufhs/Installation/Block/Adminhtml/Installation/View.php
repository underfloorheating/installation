<?php

/**
 * Installation Data Rooms
 *
* @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */
class Ufhs_Installation_Block_Adminhtml_Installation_View extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct()
	{
		// load up core objects
		$id = $this->getRequest()->getParam('id');
		$customer = Mage::getModel('installation/customer')->load($id);
		$status = Mage::getModel('installation/status')->load($customer->getStatusId());
		$currentUser = Mage::getSingleton('admin/session')->getUser()->getUserId();
		$roleName = Mage::getModel('admin/user')->load($currentUser)->getRole()->getData()['role_name'];
		$statusCode = $status->getStatusCode();

		// Set Magento required layout elements
		$this->_controller = 'adminhtml_installation_view';
		$this->_blockGroup = 'installation';
		$this->_headerText = Mage::helper('installation')->__('Job #' . $id . ': ' . $status->getStatusText());
		parent::__construct();

		// Add / Remove default buttons
		$this->_removeButton('add');
		$this->_addButton('back', array(
			'label'   => Mage::helper('installation')->__('Back'),
			'onclick' => "setLocation('{$this->getUrl('*/*/index')}')",
			'class'   => 'back'
			));

		// Add role / status specific buttons
		if ($roleName != 'Installer') {
			switch ($statusCode) {
				case 'awaiting_confirmation':
				$this->_buttonAdd($id, 'performstatechange/state/' . $this->_getStatusId('with_surveyor'), 'Request Survey');
				$this->_buttonAdd($id, 'performstatechange/state/' . $this->_getStatusId('quote_with_customer'), 'Quote With Customer');
				break;
				case 'with_surveyor':
				$this->_buttonAdd($id, 'performstatechange/state/' . $this->_getStatusId('quote_required'), 'Survey Completed');
				break;
				case 'quote_required':
				$this->_buttonAdd($id, 'performstatechange/state/' . $this->_getStatusId('quote_with_customer'), 'Quote Sent');
				break;
				case 'quote_with_customer':
				$this->_buttonAdd($id, 'performstatechange/state/' . $this->_getStatusId('installation_in_progress'), 'Book Installer');
				$this->_buttonAdd($id, 'performstatechange/state/' . $this->_getStatusId('with_surveyor'), 'Request Survey');
				break;
				case 'installation_in_progress':
				$this->_buttonAdd($id, 'performstatechange/state/' . $this->_getStatusId('ready_for_billing'), 'Installation Complete');
				break;
				case 'ready_for_billing':
				$this->_buttonAdd($id, 'performstatechange/state/' . $this->_getStatusId('with_accounts'), 'Pay Installer');
				break;
				case 'with_accounts':
				$this->_buttonAdd($id, 'performstatechange/state/' . $this->_getStatusId('complete'), 'Installer Paid');
				break;
				default:;
				break;
			}
		} else {
			switch ($statusCode) {
				case 'with_surveyor':
				$this->_buttonAdd($id, 'performstatechange/state/' . $this->_getStatusId('quote_required'), 'Survey Completed');
				break;
				case 'installation_in_progress':
				$this->_buttonAdd($id, 'performstatechange/state/' . $this->_getStatusId('ready_for_billing'), 'Installation Complete');
				break;
				default:;
				break;
			}
		}

		// The cancel button has to appear last...
		if ($roleName == 'Install Manager') {
			$this->_buttonAdd($id, 'cancelled', 'Cancel', 'delete');
		} else {
			$this->_buttonAdd($id, 'requestcancel', 'Request Cancellation', 'delete');
		}

		if ($roleName == 'Administrators' || $roleName == 'Install Manager') {
			$this->_addButton('deletejob', array(
				'label'   => Mage::helper('installation')->__('Delete'),
				'onclick' => "if(confirm('Clicking \'okay\' will completely destroy everything related to this job, with no way of getting it back. Are you absolutely sure you want to do this? Are you sure you don\'t want to just cancel this job?')){setLocation('" . $this->getUrl('*/*/deletejob', array('id'=>$id)) . "')}",
				'class'   => 'delete'
				));
		}
	}

	private function _buttonAdd($id,$url,$text,$class = 'okay')
	{
		$this->_addButton($url, array(
			'label'   => Mage::helper('installation')->__($text),
			'onclick' => "setLocation('{$this->getUrl('*/*/'.$url,array('id'=>$id))}')",
			'class'   => $class
			));
	}

	private function _getStatusId($code)
	{
		return Mage::getModel('installation/status')->load($code, 'status_code')->getId();
	}
}