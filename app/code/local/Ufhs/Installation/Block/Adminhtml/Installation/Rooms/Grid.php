<?php

/**
 * Installation Data Grid
 *
* @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */
class Ufhs_Installation_Block_Adminhtml_Installation_Rooms_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('installationRoomsGrid');
		$this->setDefaultSort('name');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
		$this->setPagerVisibility(false);
		$this->setFilterVisibility(false);
	}

	protected function _prepareCollection()
	{
		$customer = Mage::registry('installation-customer-details');

		$collection = Mage::getModel('installation/room')->getCollection()
		->addFieldToFilter('quote_id',$customer->getId());
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns()
	{
		$this->addColumn('name', array(
			'header' => Mage::helper('installation')->__('Room Name'),
			'align' => 'left',
			'width' => '10px',
			'index' => 'room_name'
			));
		$this->addColumn('subfloor', array(
			'header' => Mage::helper('installation')->__('Subfloor'),
			'align' => 'left',
			'width' => '50px',
			'index' => 'subfloor'
			));
		$this->addColumn('height', array(
			'header' => Mage::helper('installation')->__('Ceiling Height'),
			'align' => 'left',
			'width' => '50px',
			'index' => 'ceiling_height'
			));
		$this->addColumn('windows', array(
			'header' => Mage::helper('installation')->__('# Windows'),
			'align' => 'left',
			'width' => '50px',
			'index' => 'number_of_windows'
			));
		$this->addColumn('area', array(
			'header' => Mage::helper('installation')->__('Floor Area'),
			'align' => 'left',
			'width' => '50px',
			'index' => 'floor_area'
			));
		$this->addColumn('finish', array(
			'header' => Mage::helper('installation')->__('Floor Finish'),
			'align' => 'left',
			'width' => '50px',
			'index' => 'floor_finish'
			));
		$this->addColumn('type', array(
			'header' => Mage::helper('installation')->__('Wall Type'),
			'align' => 'left',
			'width' => '50px',
			'index' => 'wall_type'
			));


		$object = new Varien_Object(array('grid_block' => $this));
		Mage::dispatchEvent("installation_block_adminhtml_installation_rooms_grid_preparecolumns", array("data" => $object));

		return parent::_prepareColumns();
	}
}