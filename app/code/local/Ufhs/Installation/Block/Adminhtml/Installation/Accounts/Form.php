<?php

/**
 * Installation Data Notes
 *
* @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */
class Ufhs_Installation_Block_Adminhtml_Installation_Accounts_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm()
	{
		$id = $this->getRequest()->getParam('id');
		$customer = Mage::getModel('installation/customer')->load($id);
		$form = new Varien_Data_Form(array(
			'id' => 'accounts',
			'action' => $this->getUrl('*/*/accounts', array('id' => $id)),
			'method' => 'post',
			'enctype' => 'multipart/form-data'
			)
		);
		$form->setUseContainer(true);
		$this->setForm($form);

		$financeset = $form->addFieldset('finance_form', [
			'legend' => Mage::helper('installation')->__('Financial Information'),
			'fieldset_container_id' => 'financial-grid'
			]);
		$accountset = $form->addFieldset('account_form', [
			'legend' => Mage::helper('installation')->__('Account Information'),
			'fieldset_container_id' => 'accounting-grid'
			]);

		$financeset->addField('total-quoted', 'text', array(
			'label' => Mage::helper('installation')->__('UFHS Quote Total (£)'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'total_quoted',
			'value' => $customer->getTotalQuoted()
			));
		$financeset->addField('total-cost', 'text', array(
			'label' => Mage::helper('installation')->__('Installer Cost (£)'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'total_cost',
			'value' => $customer->getTotalCost()
			));

		$accountset->addField('acc-number', 'text', array(
			'label' => Mage::helper('installation')->__('OrderWise Account Number'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'acc_number',
			'value' => $customer->getAccNumber()
			));

		$form->addField('addnewting', 'submit', array(
			'class' => 'form-button',
			'value' => 'Update Accounts'
			));

		return parent::_prepareForm();
	}
}