<?php

/**
 * Installation Data Rooms
 *
* @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */
class Ufhs_Installation_Block_Adminhtml_Installation_Rooms extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct()
	{
		$this->_controller = 'adminhtml_installation_rooms';
		$this->_blockGroup = 'installation';
		$this->_headerText = Mage::helper('installation')->__('Rooms');
        parent::__construct();
        $this->_removeButton('add');
    }
}