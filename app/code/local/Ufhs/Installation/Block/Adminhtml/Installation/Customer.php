<?php

/**
 * Installation Data Customer
 *
* @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */
class Ufhs_Installation_Block_Adminhtml_Installation_Customer extends Mage_Adminhtml_Block_Widget
{
	public function __construct()
	{
		parent::_construct();
	}
	/**
	 * Get Customer
	 * ------------
	 * Load the customer details from the Magento registry.
	 *
	 * @return	Array	The customer details.
	 */
	public function getCustomer()
	{
		return Mage::registry('installation-customer-details');
	}
	/**
	 * Get Status Text
	 * ---------------
	 * Get the customer's status text based on the customer's status ID.
	 *
	 * @return	String	The customer's status text.
	 */
	public function getStatusText()
	{
		return Mage::getModel('installation/status')->load($this->getCustomer()->getStatusId())->getStatusText();
	}
	/**
	 * Get Drawings
	 * ------------
	 * Returns a collection of the give customer's provided files.
	 *
	 * @return	Mage_Core_Model_Resource_Db_Collection_Abstract	A collection of
	 * customer files.
	 */
	public function getDrawings()
	{
		$id = $this->getCustomer()->getId();
		$collection = Mage::getModel('installation/additions')->getCollection()
		->addFieldToFilter('type','drawing')
		->addFieldToFilter('fkid',$id);
		return $collection;
	}

	public function getInstaller()
	{
		$id = $this->getCustomer()->getInstallerId();
		return Mage::getModel('installation/installers')->load($id);
	}

	public function getAdvisor()
	{
		$id = $this->getCustomer()->getAdvisorId();
		return Mage::getModel('installation/advisors')->load($id);
	}

	public function getCustomerEditUrl()
	{
		$id = $this->getRequest()->getParam('id');
		return $this->getUrl('*/*/editcustomer', ['id' => $id]);
	}

	public function getInstallerEditUrl()
	{
		$id = $this->getRequest()->getParam('id');
		return $this->getUrl('*/*/editinstaller', ['id' => $id]);
	}

	public function getAdvisorEditUrl()
	{
		$id = $this->getRequest()->getParam('id');
		return $this->getUrl('*/*/editadvisor', ['id' => $id]);
	}

	public function getInstallDateUrl()
	{
		$id = $this->getRequest()->getParam('id');
		return $this->getUrl('*/*/updateinstalldate');
	}

	public function isUserInstaller()
	{
		$currentUser = Mage::getSingleton('admin/session')->getUser()->getUserId();
		$roleName = Mage::getModel('admin/user')->load($currentUser)->getRole()->getData()['role_name'];
		return $roleName == 'Install Manager' || $roleName == 'Administrators';
	}
}