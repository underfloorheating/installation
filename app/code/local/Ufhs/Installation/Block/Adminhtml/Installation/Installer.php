<?php

/**
 * Installation Data Installer
 *
* @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */
class Ufhs_Installation_Block_Adminhtml_Installation_Installer extends Mage_Adminhtml_Block_Widget
{
    public function __construct()
    {
        parent::_construct();
    }

    private function _getId()
    {
        return $this->getRequest()->getParam('id');
    }

    public function isExistingInstaller()
    {
        return $this->getRequest()->getParam('id');
    }

    public function getReliabilityMatrix()
    {
        return Mage::getModel('installation/installerrating')->getReliabilityMatrix($this->_getId());
    }

    public function getServiceMatrix()
    {
        return Mage::getModel('installation/installerrating')->getServiceMatrix($this->_getId());
    }

    public function getProfitMatrix()
    {
        return Mage::getModel('installation/installerrating')->getProfitMatrix($this->_getId());
    }

    public function getOverallRating()
    {
        return Mage::getModel('installation/installerrating')->getOverallRating($this->_getId());
    }

    public function getRanking()
    {
        return Mage::getModel('installation/installerrating')->getRanking($this->_getId());
    }

}