<?php

/**
 * Installation Chart
 *
* @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */
class Ufhs_Installation_Block_Adminhtml_Installation_Report_Chart extends Mage_Adminhtml_Block_Widget
{
	public function __construct()
	{
		parent::_construct();
	}
	/**
	 * Quantity Pie
	 * ------------
	 * Returns the data required for the quantity pie chart.
	 *
	 * @return	Array	An array containing the quantity data in the pie chart format.
	 */
	public function quantityPie()
	{
		$return = [];
		$collection = Mage::getModel('installation/status')->getCollection();
		foreach($collection as $item)
		{
			if($quantity = $this->_getStatusQuantity($item->getId()) > 0)
			{
				$return[$item->getStatusText()] = $this->_getStatusQuantity($item->getId());
			}
		}
		return $return;
	}
	/**
	 * Total Pie
	 * ---------
	 * Returns the total number of entries in the customer collection.
	 *
	 * @return	Int	The total number fo entries in the customer collection.
	 */
	public function totalPie()
	{
		$collection = $this->_getCollection('customer');
		return count($collection) > 0 ? count($collection) : 0;
	}
	/**
	 * Get Collection
	 * --------------
	 * A centralised function for getting a particular collection with the inputted
	 * date range applicable.
	 *
	 * @param	$string	String	The collection name.
	 * @return	Mage_Core_Model_Resource_Db_Collection_Abstract	The collection.
	 */
	private function _getCollection($status)
	{
		return Mage::getModel('installation/'.$status)->getCollection()
		->addFieldToFilter('state_change',array('gteq' => Mage::registry('installation-report-datefrom')))
		->addFieldToFilter('state_change',array('lteq' => Mage::registry('installation-report-dateto')));
	}
	/**
	 * Month Bar Profit
	 * ----------------
	 * Returns the profit data for the monthly bar chart.
	 *
	 * @return	Array	An array containing the profit data in the bar chart format.
	 */
	public function monthBarProfit()
	{
		$return = [0,0,0,0,0,0,0,0,0,0,0,0];
		$collection = $this->_getCollection('customer')->addFieldToFilter('status_id',8);
		foreach($collection as $item)
		{
			$date = $item->getStateChange();
			$d = date_parse_from_format("Y-m-d", $date);
			$return[$d['month'] - 1] += $item->getTotalQuoted() - $item->getTotalCost();
		}
		return $return;
	}
	/**
	 * Month Bar Cost
	 * --------------
	 * Returns the cost data for the monthly bar chart.
	 *
	 * @return	Array	An array containing the cost data in the bar chart format.
	 */
	public function monthBarCost()
	{
		$return = [0,0,0,0,0,0,0,0,0,0,0,0];
		$collection = $this->_getCollection('customer')->addFieldToFilter('status_id',8);
		foreach($collection as $item)
		{
			$date = $item->getStateChange();
			$d = date_parse_from_format("Y-m-d", $date);
			$return[$d['month'] - 1] += $item->getTotalCost();
		}
		return $return;
	}
	/**
	 * Get Status Quantity
	 * -------------------
	 * Returns the total number of entries in the customer collection for a
	 * particular status.
	 *
	 * @param	$statusId	Int	The particular status's ID.
	 * @return	Int	The quantity of that particular status.
	 */
	private function _getStatusQuantity($statusId)
	{
		$collection = $this->_getCollection('customer')->addFieldToFilter('status_id',$statusId);
		return count($collection) > 0 ? count($collection) : 0;
	}
}