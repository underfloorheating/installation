<?php
class Ufhs_Installation_Block_Adminhtml_Installation_Cancellation extends Mage_adminhtml_Block_Widget_Form_Container
{
    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function __construct()
    {
        parent::__construct();
        $id = $this->getRequest()->getParam('id');
        $this->_objectId = 'id';
        $this->_blockGroup = 'installation';
        $this->_controller = 'adminhtml';
        $this->_mode = 'installation_cancellation';

        $this->_removeButton('save');
        $this->_removeButton('delete');
        $this->_removeButton('back');
        $this->_removeButton('reset');

        $this->addButton('new_back', [
            'label' => 'Back',
            'onclick' => "setLocation('" . $this->getUrl('*/*/view/id/' . $id) . "')",
            'class' => 'back'
            ]);

        $this->addButton('new_save', [
            'label' => 'Submit',
            'onclick' => "document.getElementById('cancellation').submit()",
            'class' => 'add'
            ]);
    }

    public function getHeaderText()
    {
        return Mage::helper('installation')->__('Cancellation Request');
    }
}