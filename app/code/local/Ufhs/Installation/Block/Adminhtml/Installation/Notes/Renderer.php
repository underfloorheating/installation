<?php
class Ufhs_Installation_Block_Adminhtml_Installation_Notes_Renderer extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
	public function render(Varien_Object $row)
	{
		$value =  $row->getData($this->getColumn()->getIndex());
		$collection = Mage::getmodel('installation/additions')->getCollection()
		->addFieldToFilter('type','attachment')
		->addFieldToFilter('fkid',$value);
		$html = '';
		foreach($collection as $item)
		{
			$filePath = '/media/ufhs/installation/attachment/' . $value . '/' . $item->content;
			$html .= '<a target="_blank" href="' . $filePath . '">'.$item->content.'</a>';
		}
		return $html;
	}
}
?>