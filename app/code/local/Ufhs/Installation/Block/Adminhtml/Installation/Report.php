<?php

/**
 * Installation Data Report
 *
* @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */
class Ufhs_Installation_Block_Adminhtml_Installation_Report extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct()
	{
		$post = $this->getRequest()->getParams();

		// If the dates have been POSTed then standardise them, otherwise use
		// default dates
		if(isset($post['datefrom']))
		{
			$datefrom = $this->_standardisedDate($post['datefrom']);
		}
		else
		{
			$datefrom = date('1999/12/31');
		}
		if(isset($post['dateto']))
		{
			$dateto = $this->_standardisedDate($post['dateto']);
		}
		else
		{
			$to = new DateTime('tomorrow');
			$dateto = $to->format('Y/m/d');
		}

		// Take whatever we've got and set it to the Mage Reigstry for later use
		$this->_setNewRegistry('installation-report-datefrom',$datefrom);
		$this->_setNewRegistry('installation-report-dateto',$dateto);

		// Setup up the page to be rendered
		$this->_controller = 'adminhtml_installation_report';
		$this->_blockGroup = 'installation';
		$this->_headerText = Mage::helper('installation')->__('Overview Report: ' . $this->_humanReadableDate($datefrom) . ' - ' . $this->_humanReadableDate($dateto));
		parent::__construct();
		$this->_removeButton('add');
	}
	/**
	 * Human Readable Data
	 * -------------------
	 * Output the given datetime in a human readable format.
	 *
	 * @param	$string	String	A datetime string.
	 * @return	DateTime	A datetime object in a human readable format.
	 */
	private function _humanReadableDate($string)
	{
		$date = new DateTime();

		return $date->createFromFormat('Y/m/d',$string)->format('D, d M Y');
	}
	/**
	 * Standarised Date
	 * ----------------
	 * Output the given datetime in a standarised DD/MM/YYYY format.
	 *
	 * @param	$string	String	A datetime string.
	 * @return	DateTime	A datetime object in a standarised format.
	 */
	private function _standardisedDate($string)
	{
		$date = new DateTime();
		$dated = $date->createFromFormat('d/m/Y',$string);
		return $dated ? $dated->format('Y/m/d') : $date->createFromFormat('d/m/Y','01/01/1990')->format('Y/m/d');
	}
	/**
	 * Set New Registry
	 * ----------------
	 * Unregister a Mage registry and re-register it with the give value.
	 *
	 * @param	$index	String	The index.
	 * @param	$value	String	The value.
	 */
	private function _setNewRegistry($index,$value)
	{
		Mage::unregister($index);
		Mage::register($index,$value);
	}
}