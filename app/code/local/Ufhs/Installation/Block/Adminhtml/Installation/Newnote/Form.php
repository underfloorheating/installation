<?php

/**
 * Installation Data Notes
 *
* @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */
class Ufhs_Installation_Block_Adminhtml_Installation_Newnote_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm()
	{
		$form = new Varien_Data_Form(array(
			'id' => 'newnote',
			'action' => $this->getUrl('*/*/note', array('id' => $this->getRequest()->getParam('id'))),
			'method' => 'post',
			'enctype' => 'multipart/form-data'
			)
		);
		$form->setUseContainer(true);
		$this->setForm($form);

		$fieldset = $form->addFieldset('newnote_form', array('legend'=>Mage::helper('installation')->__('New Note')));

		$fieldset->addField('comment', 'textarea', array(
			'label' => Mage::helper('installation')->__('Comment'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'comment'
			));
		$fieldset->addField('attachment','file',array(
			'label' => Mage::helper('installation')->__('Attach a file'),
			'name' => 'attachment',
			'required' => false
        	));

        $fieldset->addField('addnew', 'submit', array(
            'class' => 'form-button',
            'value' => 'Add New Note'
            ));

		return parent::_prepareForm();
	}
}