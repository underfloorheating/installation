<?php

/**
 * Installation Report Date
 *
* @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */
class Ufhs_Installation_Block_Adminhtml_Installation_Report_Date_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm()
	{
		$form = new Varien_Data_Form(array(
			'id' => 'filterdate',
			'action' => $this->getUrl('*/*/*/'),
			'method' => 'get',
			'enctype' => 'plain/text'
			)
		);
		$form->setUseContainer(true);
		$this->setForm($form);
		$form->addField('datefrom','date',array(
			'name' => 'datefrom',
			'label' => Mage::helper('installation')->__('From'),
			'tabindex' => 1,
			'image' => $this->getSkinUrl('images/grid-cal.gif'),
			'format' => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
			'value' => date(Mage::registry('installation-report-datefrom'))
			));
		$form->addField('dateto','date',array(
			'name' => 'dateto',
			'label' => Mage::helper('installation')->__('To'),
			'tabindex' => 1,
			'image' => $this->getSkinUrl('images/grid-cal.gif'),
			'format' => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
			'value' => date(Mage::registry('installation-report-dateto'))
			));
		$form->addfield('submit','submit',array(
			'value' => 'Filter',
			'class' => 'form-button'
			));

		return parent::_prepareForm();
	}
}